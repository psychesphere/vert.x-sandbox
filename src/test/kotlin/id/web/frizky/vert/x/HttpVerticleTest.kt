package id.web.frizky.vert.x

import io.restassured.RestAssured.given
import io.restassured.response.Response
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient.create
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@Extensions(
    ExtendWith(VertxExtension::class)
)
internal class HttpVerticleTest {
    @BeforeEach
    internal fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        println("START UP")
        vertx.deployVerticle(HttpVerticle::class.qualifiedName, testContext.completing())
    }

    @Test
    internal fun test_with_webclient_vertx(vertx: Vertx, testContext: VertxTestContext) {
        println("run: test_me_vertx")
        assertNotNull(vertx)
        assertNotNull(testContext)
        val webClient = create(vertx)
        webClient.get(9090, "localhost", "/").send(testContext.succeeding { response: HttpResponse<Buffer> ->
            testContext.verify {
                assertEquals(200, response.statusCode())
                assertEquals("OK", response.bodyAsString())
            }
            testContext.completeNow()
        })
    }

    @Test
    internal fun test_with_restassured(vertx: Vertx, testContext: VertxTestContext) {
        given().get("http://localhost:9090").then().statusCode(200)
        testContext.completeNow()
    }
}