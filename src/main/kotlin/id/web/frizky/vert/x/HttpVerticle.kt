package id.web.frizky.vert.x

import io.vertx.core.AbstractVerticle

class HttpVerticle : AbstractVerticle() {
    override fun start() {
        val httpServer = vertx.createHttpServer()
        httpServer.requestHandler { request ->
            request.response().end("OK")
        }.listen(config().getInteger("http.port", 9090))
    }
}