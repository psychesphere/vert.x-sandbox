package id.web.frizky.vert.x

import io.vertx.core.Vertx
import io.vertx.ext.dropwizard.DropwizardMetricsOptions
import io.vertx.core.VertxOptions


fun main() {
    val vertx = Vertx.vertx(
        VertxOptions().setMetricsOptions(
            DropwizardMetricsOptions().setEnabled(true).setJmxEnabled(true)
        )
    )
    vertx.deployVerticle(HttpVerticle::class.qualifiedName)
}