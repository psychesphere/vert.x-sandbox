package id.web.frizky.vert.x.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.full.companionObject

interface Loggable {
    // Return logger for Java class, if companion object fix the name
    fun <T: Any> logger(forClass: Class<T>): Logger {
        return LoggerFactory.getLogger(unwrapCompanionClass(forClass).name.toString())
    }

    // unwrap companion class to enclosing class given a Java Class
    fun <T : Any> unwrapCompanionClass(ofClass: Class<T>): Class<*> {
        return ofClass.enclosingClass?.takeIf {
            ofClass.enclosingClass.kotlin.companionObject?.java == ofClass
        } ?: ofClass
    }

    // return logger from extended class (or the enclosing class)
    fun <T: Any> T.logger(): Logger {
        return logger(this.javaClass)
    }

    // abstract base class to provide logging, intended for companion objects more than classes but works for either
    abstract class WithLogging: Loggable {
        val logger = logger()
    }
}
